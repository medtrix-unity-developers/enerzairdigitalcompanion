﻿using NVS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapToAnimate : MonoBehaviour
{
    public enum Mode
    {
        ToggleBool,
        SetTrigger
    }

    bool value = false;
    public string Key;
    public Animator animator;
    public bool resetRotation = false;
    public bool toggleLabelsOnFalse = false;

    public Mode mode;

    private void OnEnable()
    {   
        //animator.Play("Entry", 0);
    }

    private void OnMouseUpAsButton()
    {
        StartCoroutine(TriggerAsync());
    }

    IEnumerator TriggerAsync()
    {
        SlideManager sm = FindObjectOfType<SlideManager>();
        if (resetRotation && !sm.labelState)
        {
            yield return sm.LerpTransformTo(Quaternion.identity, sm.arState ? sm.GetARScale() : Vector3.one, true);
        }

        DoTrigger();
        SendMessage("OnTap", SendMessageOptions.DontRequireReceiver);
    }

    void DoTrigger()
    {
        switch (mode)
        {
            case Mode.ToggleBool:
                animator.SetBool(Key, !value);
                value = !value;
                break;
            case Mode.SetTrigger:
                animator.SetTrigger(Key);
                break;
        }
    }
}
