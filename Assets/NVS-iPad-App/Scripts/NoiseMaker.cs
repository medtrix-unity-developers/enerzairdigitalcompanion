﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour
{
    private void OnMouseUpAsButton()
    {
        GetComponent<AudioSource>().Play();

        SendMessage("OnTap", SendMessageOptions.DontRequireReceiver);
    }
}
