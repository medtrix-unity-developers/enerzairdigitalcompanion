﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NVS
{
    public class PhoneScreen : MonoBehaviour
    {
        public Texture2D[] screens;
        public GameObject[] associatedLabels;
        public Vector3[] associatedHotspots;
        public Transform hotspotMarker;
        public MeshRenderer meshRenderer;
        public float tapMoveThreshold = 0.05f;

        int screenIndex = 0;

        Vector2 mouseStartPos = Vector3.zero;
        bool detectedMouseDown = false;

        // Start is called before the first frame update
        void Start()
        {
            if (meshRenderer == null)
                meshRenderer = GetComponent<MeshRenderer>();

            SetScreen(0);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnMouseDown()
        {
            detectedMouseDown = true;
            mouseStartPos = Input.mousePosition;
        }

        private void OnMouseUpAsButton()
        {
            if (detectedMouseDown && Vector2.Distance(mouseStartPos, Input.mousePosition) <= tapMoveThreshold)
            {
                screenIndex++;

                if (screenIndex >= screens.Length)
                    screenIndex = 0;

                SetScreen(screenIndex);

                SendMessage("OnTap", SendMessageOptions.DontRequireReceiver);
            }

            detectedMouseDown = false;
        }

        void SetScreen(int index)
        {
            screenIndex = index;
            meshRenderer.materials.First(m => m.name.Contains("SCREEN")).SetTexture("_MainTex", screens[index]);

            if(associatedLabels != null)
            {
                foreach(GameObject go in associatedLabels)
                {
                    go.SetActive(false);
                }

                if (associatedLabels.Length > index)
                    associatedLabels[index].SetActive(true);
            }
            
            if(associatedHotspots != null)
            {
                if (associatedHotspots.Length > index)
                    hotspotMarker.localPosition = associatedHotspots[index];
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (associatedHotspots == null)
                return;

            foreach(Vector3 v in associatedHotspots)
            {
                Gizmos.DrawSphere(transform.TransformPoint(v), 0.005f);
            }
        }
    }
}