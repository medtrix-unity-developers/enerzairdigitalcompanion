﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideHotspot : MonoBehaviour
{
    public GameObject hotspot;

    private void OnEnable()
    {
        hotspot.SetActive(true);
    }

    void OnTap()
    {
        hotspot.SetActive(false);
    }
}
