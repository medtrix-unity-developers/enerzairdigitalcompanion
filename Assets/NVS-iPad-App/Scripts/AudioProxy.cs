﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioProxy : MonoBehaviour
{
    public AudioSource source;

    public AudioClip clipClick;
    public AudioClip clipWhirr;

    private void Start()
    {
        if (source == null)
            source = GetComponent<AudioSource>();
    }

    public void Play()
    {
        if(source != null && source.clip != null)
            source.Play();
    }

    public void PlayClick()
    {
        if(source != null)
            source.PlayOneShot(clipClick);
    }

    public void PlayWhirr()
    {
        if (source != null)
            source.PlayOneShot(clipWhirr);
    }
}
