﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NVS
{
    public class Slide : MonoBehaviour
    {
        [Header("Targets")]
        public Transform slideContentHolder;
        public Transform slideContent;
        public Transform labelContent;

        [Header("Settings"), Multiline]
        public string headerText;
        [Multiline]
        public string headerSubText;
        [LabelText("AR Capable")]
        public bool arCapable = false;
        public bool hasLabels = false;
        public int topRightIconIndex = -1;
        public bool isGestureDriven = true;
        public bool showReset = true;
        public bool showHome = true;
        public bool forceAR = false;
       

        [LabelText("AR Full Rotation"), Header("Rotation Settings"), ShowIf("isGestureDriven")]
        public bool arFullRotation = true;
        [ShowIf("isGestureDriven")]
        public bool limitRotation = false;
        [ShowIf("DoRotationLimit")]
        public Vector3 rotationMin;
        [ShowIf("DoRotationLimit")]
        public Vector3 rotationMax;

        [Header("Zoom Settings"), ShowIf("isGestureDriven")]
        public float maxZoom = 3f;
        [ShowIf("isGestureDriven")]
        public float minZoom = 1f;

        bool DoRotationLimit
        {
            get { return isGestureDriven && limitRotation; }
        }

        public UnityEvent onSlideAppear;
        public UnityEvent onSlideDisappear;
        public UnityEvent onSlideReset;

        // Start is called before the first frame update
        void Start()
        {
            if (slideContent == null)
                slideContent = transform.GetChild(0);
        }

        public Bounds GetContentBounds()
        {
            Bounds fullBounds = new Bounds();
            bool first = true;

            foreach(Renderer r in GetComponentsInChildren<Renderer>())
            {
                if (first)
                {
                    fullBounds = new Bounds(r.bounds.center, r.bounds.size);
                    first = false;
                }
                else
                {
                    fullBounds.Encapsulate(r.bounds);
                }
            }


            return fullBounds;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}