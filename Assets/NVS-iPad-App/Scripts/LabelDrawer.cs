﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelDrawer : MonoBehaviour
{
    public Transform labelAnchor;
    public Transform labelTarget;
    public RectTransform lineDot;
    public RectTransform fakeLine;
    public float distanceFromCamera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        Update();
    }

    private void OnDisable()
    {
        lineDot.position = new Vector3(1000f, 1000f, 1000f);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 screenPoint = Vector3.zero;
        if (!labelTarget.gameObject.activeInHierarchy)
        {
            screenPoint = new Vector3(1000f, 1000f, 0f);
        }
        else
        {
            screenPoint = Camera.main.WorldToScreenPoint(labelTarget.position);
        }

        lineDot.position = new Vector3(screenPoint.x, screenPoint.y, distanceFromCamera);

        Canvas.ForceUpdateCanvases();

        /*
        Vector3 diff = labelAnchor.localPosition - lineDot.localPosition;

        fakeLine.sizeDelta = new Vector2(diff.magnitude, fakeLine.sizeDelta.y);

        fakeLine.pivot = new Vector2(0f, 0.5f);
        fakeLine.localPosition = labelAnchor.localPosition;
        fakeLine.localRotation = Quaternion.Euler(0, 0, (Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg) + 180f);
        */
    }
}
