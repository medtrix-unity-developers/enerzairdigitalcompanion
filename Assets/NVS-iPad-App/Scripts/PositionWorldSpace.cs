﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionWorldSpace : MonoBehaviour
{
    public Transform target;
    RectTransform rectTransform;
    Canvas canvas;

    IEnumerator delayedShowCoroutine;
    bool wasHidden = true;

    float hideCounter = 0.2f;

    private void OnEnable()
    {
        if(rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
            canvas = GetComponentInParent<Canvas>();
        }

        PositionUpdate();
    }

    private void OnDisable()
    {
        hideCounter = 0.2f;
        rectTransform.position = new Vector3(-10000f, -10000f, -10000f);
    }

    // Update is called once per frame
    void Update()
    {
        PositionUpdate();
    }

    void PositionUpdate()
    {
        if (!target.gameObject.activeInHierarchy)
        {
            rectTransform.position = new Vector3(-10000f, -10000f, -10000f);
        }
        else
        {
            if (hideCounter > 0f)
            {
                hideCounter -= Time.deltaTime;
                rectTransform.position = new Vector3(-10000f, -10000f, -10000f);
            }
            else
            {
                Vector3 p = Camera.main.WorldToScreenPoint(target.position);
                transform.position = new Vector3(p.x, p.y, 0f);
            }
        }

        Canvas.ForceUpdateCanvases();
    }
}
