﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using PaperPlaneTools;

 
public class JailBreakDetector : MonoBehaviour
{
    //[DllImport("__Internal")]
    //private static extern bool isJailbreak();

   /* public static bool DetectIsJailbreak()
    {
        #if UNITY_EDITOR
	        return false;
        #else
            return isJailbreak();
        #endif
    }*/

    public static bool CheckManualJailBreak()
    {

#if UNITY_EDITOR
        return false;
#else
         string[] paths = new string[10] {
             "/Applications/Cydia.app",
             "/private/var/lib/cydia",
             "/private/var/tmp/cydia.log",
             "/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",
             "/usr/libexec/sftp-server",
             "/usr/bin/sshd",
             "/usr/sbin/sshd",
             "/Applications/FakeCarrier.app",
             "/Applications/SBSettings.app",
             "/Applications/WinterBoard.app",};
            int i;
            bool jailbroken = false;
            for (i = 0; i < paths.Length; i++)
            {
                if (System.IO.File.Exists(paths[i])){
                    jailbroken = true;
                }
            }
            PlayerPrefs.SetInt("JailBreak", 1);

           return jailbroken;
#endif
    }


    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("JailBreak") == 1) return;
 
        if (CheckManualJailBreak() == true)  // DetectIsJailbreak() == true || 
        {
            new Alert("JailBreak", "This device is Jailbroken.")
               .SetPositiveButton("CONTINUE", () => {
                   Debug.Log("Ok handler");
               })
               .Show();
 
        }
          
        //Debug.Log("IS JAIL BREAK... "+ DetectIsJailbreak() + CheckManualJailBreak());
    }

   
}
