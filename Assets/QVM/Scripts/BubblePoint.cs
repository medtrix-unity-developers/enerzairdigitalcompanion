﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QVM
{
    public class BubblePoint : MonoBehaviour
    {
        public Vector3 influence = Vector3.one;
        public float triggerAngle = 30f;
        public float currentAngle = 0f;

        Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 influenceForward = new Vector3(transform.forward.x * influence.x, transform.forward.y * influence.y, transform.forward.z * influence.z);

            Vector3 cameraDirection = new Vector3((Camera.main.transform.position.x - transform.position.x) * influence.x, (Camera.main.transform.position.y - transform.position.y) * influence.y, (Camera.main.transform.position.z - transform.position.z) * influence.z);

            float currentAngle = Vector3.Angle(influenceForward, cameraDirection);

            animator.SetBool("Show", currentAngle <= triggerAngle);
        }
    }
}